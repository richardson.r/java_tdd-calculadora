package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumerosInteiros() {
        int resultado = Calculadora.somar(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomaDeDoisNumerosQuabrados() {
        Double resultado = Calculadora.somar(2.3, 2.4);
        Assertions.assertEquals(4.7, resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosInteiros() {
        int resultado = Calculadora.subtrair(3, 1);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testarSubtracaoDeNumerosQuebrados() {
        Double resultado = Calculadora.subtrair(2.2, 0.1);
        Assertions.assertEquals(2.1, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteiros() {
        int resultado = Calculadora.dividir(12, 3);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarDivisaoDeNumerosQuebrados() {
        Double resultado = Calculadora.dividir(2.2, 0.4);
        Assertions.assertEquals(5.5, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosInteiros() {
        int resultado = Calculadora.multiplicar(3, 2);
        Assertions.assertEquals(6, resultado);
    }

    @Test
    public void testarMultiplicacaoDeNumerosQuebrados() {
        Double resultado = Calculadora.multiplicar(2.2, 1.1);
        Assertions.assertEquals(2.42, resultado);
    }
}
