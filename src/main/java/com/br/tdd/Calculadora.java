package com.br.tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public static int somar(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static Double somar(Double primeiroNumero, Double segundoNumero){
        String num1 = primeiroNumero.toString();
        String num2 = segundoNumero.toString();
        BigDecimal resultado = new BigDecimal(num1).add(new BigDecimal(num2));

        return resultado.doubleValue();
    }

    public static int subtrair(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero - segundoNumero;
        return resultado;
    }

    public static Double subtrair(Double primeiroNumero, Double segundoNumero){
        String num1 = primeiroNumero.toString();
        String num2 = segundoNumero.toString();
        BigDecimal resultado = new BigDecimal(num1).subtract(new BigDecimal(num2));

        return resultado.doubleValue();
    }

    public static int dividir(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public static Double dividir(Double primeiroNumero, Double segundoNumero){
        String num1 = primeiroNumero.toString();
        String num2 = segundoNumero.toString();
        BigDecimal resultado = new BigDecimal(num1).divide(new BigDecimal(num2),8,RoundingMode.UP);

        return resultado.doubleValue();
    }

    public static int multiplicar(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public static Double multiplicar(Double primeiroNumero, Double segundoNumero){
        String num1 = primeiroNumero.toString();
        String num2 = segundoNumero.toString();
        BigDecimal resultado = new BigDecimal(num1).multiply(new BigDecimal(num2));

        return resultado.doubleValue();
    }
}
